import firebase from "firebase/compat/app";

// import "firebase/database";
import "firebase/compat/auth";
import { getFirestore } from "@firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyDONLqc9QA8i8TSaAiP-ZJkmIgOJwENEDI",
  authDomain: "fsw-16-dua.firebaseapp.com",
  projectId: "fsw-16-dua",
  storageBucket: "fsw-16-dua.appspot.com",
  messagingSenderId: "1098140799308",
  appId: "1:1098140799308:web:b7dba75afe6e92ac5fffe9"
 };

const db= getFirestore(firebase.initializeApp(firebaseConfig));

// const db = firebaseDB.database().ref();
const auth = firebase.auth();
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
const facebookAuthProvider = new firebase.auth.FacebookAuthProvider();
const githubAuthProvider = new firebase.auth.GithubAuthProvider();


export { auth, googleAuthProvider, facebookAuthProvider, githubAuthProvider, db};
