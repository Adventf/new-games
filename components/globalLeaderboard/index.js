import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/dist/client/link'
import { useDispatch, useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import { useState, useEffect } from "react";
import { db } from '../../services/firebase-config';
import {
    collection,
    getDocs,
    addDoc,
    updateDoc,
    deleteDoc,
    doc,
    query,
    orderBy,
    where
  } from "firebase/firestore";
import styles from './globalLeaderboard.module.css'
import {PopoverHeader, PopoverBody, UncontrolledPopover} from 'reactstrap'



const GlobalLeaderboard = () => {
    const [users, setUsers] = useState([]);
    const usersCollectionRef = collection(db, "users");
    const q = query(usersCollectionRef, orderBy("score","desc"))
    const router = useRouter()

    useEffect(() => {
      const getUsers = async () => {
        const data = await getDocs(q);
        setUsers(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
      };
  
      getUsers();
    }, []);

    const [curUser, setCurUser] = useState({})  
    const {currentUser} = useSelector((state) => state.user);
    const dispatch = useDispatch();
    useEffect(() => {
      const getUser= async () => {
          const data = await getDocs(usersCollectionRef)
          let allUsers = data.docs.map((doc) => ({...doc.data(), id: doc.id}));
          setCurUser(allUsers.find(x => x.email == currentUser.email))

      }
      getUser()
    }, [])

    const [randScore, setRandScore] = useState(0);

    const randomGet = async (id, score, scoreSuit) => {
        const ValorMax = 100
        const ValorMin = 0
        const newNumber = parseInt(Math.random() * (ValorMax - ValorMin)) + ValorMin;
    setRandScore(newNumber);
    if (ValorMax < ValorMin) {
      alert('Max value should be higher than Min value')
      setRandScore('Error')
    };
    const userDoc = doc(db, "users", id);
    const newFields = { 
        score: score + newNumber, 
        scoreSuit: scoreSuit + newNumber };
    await updateDoc(userDoc, newFields);
    }

    const randomGet2 = async (id, score, scoreDummy1) => {
        const ValorMax = 100
        const ValorMin = 0
        const newNumber = parseInt(Math.random() * (ValorMax - ValorMin)) + ValorMin;
    setRandScore(newNumber);
    if (ValorMax < ValorMin) {
      alert('Max value should be higher than Min value')
      setRandScore('Error')
    };
    const userDoc = doc(db, "users", id);
    const newFields = { 
        score: score + newNumber, 
        scoreDummy1: scoreDummy1 + newNumber };
    await updateDoc(userDoc, newFields);
    }

    const randomGet3 = async (id, score, scoreDummy2) => {
        const ValorMax = 100
        const ValorMin = 0
        const newNumber = parseInt(Math.random() * (ValorMax - ValorMin)) + ValorMin;
    setRandScore(newNumber);
    if (ValorMax < ValorMin) {
      alert('Max value should be higher than Min value')
      setRandScore('Error')
    };
    const userDoc = doc(db, "users", id);
    const newFields = { 
        score: score + newNumber, 
        scoreDummy2: scoreDummy2 + newNumber };
    await updateDoc(userDoc, newFields);
    }

    // const [rankNow, setRankNow] = useState(1);
    let rankNow = 0;
    const assignRank = async(id) => {
        const userDoc = doc(db, "users", id);
        const rankFields = {rank: rankNow}
        await updateDoc(userDoc, rankFields)
    }

    return (
      <div className={styles.main}>
        <div className="row">
          <div className="col">
            <h2 className={styles.judul}>GLOBAL LEADERBOARD</h2>
        </div>
        </div>

        <table className={styles.table}>
					<tr className={styles.tr}>
						<th className={styles.th}>Rank</th>
						<th className={styles.th}>Username</th>
						<th className={styles.th}>Total Score</th>
                        <Link href="/leaderboard"><th className={styles.th}>Game Suit</th></Link>
                        <th className={styles.th}>Dummy1</th>
                        <th className={styles.th}>Dummy2</th>
					</tr>
          {users.map((user) => {
              rankNow++
              assignRank(user.id)
              if (user.email == currentUser.email){
                  return(
                    <tr className={styles.tr}>
                    <td className={styles.td2}>{user.rank}</td>
                    <Link href="/profile"><td className={styles.td2}>{user.userName}</td></Link>
                    <td className={styles.td2}>{user.score}</td>
                    <td className={styles.td2}>{user.scoreSuit}</td>  
                    <td className={styles.td2}>{user.scoreDummy1}</td>  
                    <td className={styles.td2}>{user.scoreDummy2}</td>                   
                  </tr>
                  )
              } else{           
          return (
             <tr className={styles.tr}>
						<td className={styles.td}>{user.rank}</td>
						<Link href={{ pathname: '/seeProfile', query: { data: JSON.stringify(user) } }} ><td className={styles.td}><a>{user.userName}</a></td></Link>
						<td className={styles.td}>{user.score}</td>
                        <td className={styles.td}>{user.scoreSuit}</td>  
                        <td className={styles.td}>{user.scoreDummy1}</td>  
                        <td className={styles.td}>{user.scoreDummy2}</td>                   
					  </tr>
          );}
          
        })}
				</table>
                <br></br>
                {/* <h2 className={styles.judul}>TOMBOL2 BUAT NGETES</h2>
                <Link href="/"><h2>reload</h2></Link>
                <button className={styles.btndetail} id="Popover1" onClick={()=>randomGet(curUser.id, curUser.score, curUser.scoreSuit)}>tes {curUser.userName} suit</button>
                <UncontrolledPopover trigger="click" placement="bottom" target="Popover1">
              <PopoverHeader> Your Score </PopoverHeader>
              <PopoverBody>
                {randScore}
              </PopoverBody>
            </UncontrolledPopover>

            <button className={styles.btndetail} id="Popover2" onClick={()=>randomGet2(curUser.id, curUser.score, curUser.scoreDummy1)}>tes {curUser.userName} dummy1</button>
                <UncontrolledPopover trigger="click" placement="bottom" target="Popover2">
              <PopoverHeader> Dummy Score </PopoverHeader>
              <PopoverBody>
                {randScore}
              </PopoverBody>
            </UncontrolledPopover>

            <button className={styles.btndetail} id="Popover3" onClick={()=>randomGet3(curUser.id, curUser.score, curUser.scoreDummy2)}>tes {curUser.userName} dummy2</button>
                <UncontrolledPopover trigger="click" placement="bottom" target="Popover3">
              <PopoverHeader> Dummy Score </PopoverHeader>
              <PopoverBody>
                {randScore}
              </PopoverBody>
            </UncontrolledPopover> */}
        <br></br>
        
      </div>
    );
}

export default GlobalLeaderboard;