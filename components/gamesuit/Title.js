import { settings } from "./configs/game";
import styles from "../gamesuit/Title.module.css";

export const Title = () => {
  return <h1 className={styles.title}>{settings.gameName}</h1>;
};
