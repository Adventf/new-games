import { settings } from "./configs/game";
import styles from "../gamesuit/Computer.module.css";
import Image from 'next/image'

export const Computer = ({ pcScore, userSelection, pcSelection, rockIcon, paperIcon, scissorsIcon, trophyIcon }) => {
  return (
    <div className={styles.computercard}>
      <h1>Computer</h1>
      {pcScore < settings.winTarget ? (
        userSelection === "" ? (
          <h3 className={styles.waitingmessage}>{settings.waitingMessage}</h3>
        ) : (
          <>
            <Image src={pcSelection === "Rock" ? rockIcon : pcSelection === "Paper" ? paperIcon : scissorsIcon} alt="icon" />
            <h3>PC selected: {pcSelection}</h3>
          </>
        )
      ) : (
        <>
          <Image src={trophyIcon} alt="trophy" />
          <h3>Victory!</h3>
        </>
      )}
    </div>
  );
};
