import styles from "../gamesuit/Message.module.css"

export const Message = ({ userSelection, message }) => {
  return (
    <div className={styles.messagebox}>
     <h2>{userSelection === "" ? "VS" : message}</h2>
    </div>
  );
};
