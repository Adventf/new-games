import styles from "../gamesuit/Score.module.css";

export const Score = ({ score }) => {
  return <h1 className={styles.score}>{score}</h1>;
};
