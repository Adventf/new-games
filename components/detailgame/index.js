import styles from '../detailgame/detail.module.css';
import Link from 'next/link';

const Detail = () => {
	const image = './assets/rockpaperstrategy-1600.jpg';
	return (
		<div className={styles.container}>
			<div className={styles.column}>
				<img style={{ width: 720 }} className={styles.img} src={image} alt="..." />
				{/* <Link href="/gamesuit">
				<button className={styles.btngame} href="">
					Play Game
				</button>
				</Link> */}
			</div>
			<div className={styles.column}>
				<h2 className={styles.judul}>BATU GUNTING KERTAS</h2>
				<hr />
				<p className={styles.p}>
					Batu-Gunting-Kertas adalah sebuah permainan tangan dua orang. Permainan ini sering digunakan untuk
					pemilihan acak, seperti halnya pelemparan koin, dadu, dan lain-lain. Beberapa permainan dan olahraga
					menggunakannya untuk menentukan peserta mana yang bermain terlebih dahulu. Kadang ia juga dipakai
					untuk menentukan peran dalam permainan peran, maupun dipakai sebagai sarana perjudian. Permainan ini
					dimainkan di berbagai belahan dunia. Di kalangan anak-anak Indonesia, permainan ini juga dikenal
					dengan istilah "Suwit Jepang". Di Indonesia dikenal juga permainan sejenis yang dinamakan suwit.
				</p>

				<Link href="/gamesuit">
					<button className={styles.btndetail}>PLAY</button>
				</Link>
				<Link className={styles.btndetail} href="/leaderboard">
					<button className={styles.btndetail}>Leader Board</button>
				</Link>
			</div>
		</div>
	);
};

export default Detail;
