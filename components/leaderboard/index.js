import styles from '../leaderboard/leaderboard.module.css';
import Link from 'next/link';
import { useDispatch, useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import { useState, useEffect } from "react";
import { db } from '../../services/firebase-config';
import {
    collection,
    getDocs,
    addDoc,
    updateDoc,
    deleteDoc,
    doc,
    query,
    orderBy,
    limit
  } from "firebase/firestore";

const Board = () => {
	const [users, setUsers] = useState([]);
    const usersCollectionRef = collection(db, "users");
    const q = query(usersCollectionRef, orderBy("scoreSuit","desc"), limit(3))
	const {currentUser} = useSelector((state) => state.user);
	const [topRanked, setTopRanked] = useState(false)
	// let topRanked = false;
	useEffect(() => {
		const getUsers = async () => {
		  const data = await getDocs(q);
		  setUsers(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
		};
	
		getUsers();
	  }, []);
	
	  const [curUser, setCurUser] = useState({})  
	  const dispatch = useDispatch();
	  useEffect(() => {
		const getUser= async () => {
			const data = await getDocs(usersCollectionRef)
			let allUsers = data.docs.map((doc) => ({...doc.data(), id: doc.id}));
			setCurUser(allUsers.find(x => x.email == currentUser.email))
  
		}
		getUser()
	  }, [])
	  
	const renderTopRank = () => {
		if(!topRanked){
			return(
				<div>
					<table className={styles.table}>
				<tr className={styles.tr}>
                    <Link href="/profile"><td className={styles.td2}>{curUser.userName}</td></Link>
                    <td className={styles.td2}>{curUser.rank}</td>
                    <td className={styles.td2}>{curUser.scoreSuit}</td>  
                  </tr>
				  </table>
				  
				</div>
				
			)}else{
				return(
				<div>
					<h3 className={styles.top3}>You are in top 3, {curUser.userName}! AWESOME</h3>
				</div>
				)
				
			}
	}

	const topRankHandle = () => {
		if(!topRanked){
			setTopRanked(true)
		}
		console.log("top rank " + topRanked.toString())
	}


	return (
		<div className={styles.container}>
			<div className={styles.column}>
				<img style={{ width: 720 }} className={styles.img} src="./group.png" alt="..." />
			</div>
			<div className={styles.column}>
				<h2 />
				<h2 className={styles.judul}>
					LEADERBOARD <br />
					BATU GUNTING KERTAS
				</h2>
				<hr />
				
				<table className={styles.table}>
					<tr className={styles.tr}>
						<th className={styles.th}>Nama</th>
						<th className={styles.th}>Rank</th>
						<th className={styles.th}>Score</th>
					</tr>
			{users.map((user) => {
              		if (user.email == currentUser.email){
                  return(
                    <tr className={styles.tr}>
                    <Link href="/profile"><td className={styles.td2}>{user.userName}</td></Link>
                    <td className={styles.td2}>{user.rank}</td>
                    <td className={styles.td2}>{user.scoreSuit}</td>  
                     {topRankHandle()}
					 
                  </tr>
                  )
              } else{           
          return (
             <tr className={styles.tr}>
						<Link href={{ pathname: '/seeProfile', query: { data: JSON.stringify(user) } }} ><td className={styles.td}><a>{user.userName}</a></td></Link>
						<td className={styles.td}>{user.rank}</td>
                        <td className={styles.td}>{user.scoreSuit}</td>                  
					  </tr>
			
          );}
          
        })}
				
				</table>
				{renderTopRank()}			
				{/* <h2 className={styles.judul}>YOU ARE TOP RANKED, {curUser.userName}! AWESOME</h2> */}
				<Link className={styles.btndetail} href="/detail">
					<button className={styles.btndetail}>Back</button>
				</Link>
				<Link className={styles.btndetail} href="/globalLeaderboard">
					<button className={styles.btndetail}>Global Leaderboard</button>
				</Link>
			</div>
			
		</div>
	);
};

export default Board;
