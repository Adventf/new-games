import React, { createContext, useState, useEffect } from 'react'
import { Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, UncontrolledCollapse, Button } from 'reactstrap';
import {onAuthStateChanged, signOut} from "firebase/auth";
import { auth } from "../../services/firebase-config";
import Link from 'next/link'
import styles from './Navbar.module.css'
import { logoutInitiate, get} from '../../redux/actions';
import { useDispatch, useSelector } from 'react-redux'
import { useRouter } from 'next/router';
import { db } from '../../services/firebase-config';
import { collection, getDocs } from "firebase/firestore";


const NavBar = () => {

	const [curUser, setCurUser] = useState({})
    const usersCollectionRef = collection(db, "users") ; 

	const {currentUser} = useSelector((state) => state.user);
  	const dispatch = useDispatch();
	const router = useRouter();

	  const logout = async () => {
		if(currentUser) {
    	dispatch(logoutInitiate())	
		router.push("/")
   		 }		
	  };

	  const getUser= async () => {
		const data = await getDocs(usersCollectionRef)
		let allUsers = data.docs.map((doc) => ({...doc.data(), id: doc.id}));
		setCurUser(allUsers.find(x => x.email == currentUser?.email))
	}
	
	

	  const renderScore = () => {
		  if(currentUser){
		  if(curUser.email != currentUser?.email){
			  console.log("rendering score")
			  getUser();
		  }
		}

	  }

	  const updateScore = () => {
			console.log("updating score")
			getUser();

	}

  	const renderAuthButton = () => {
	  console.log("user now :" + curUser?.email)
	  renderScore()

	if (!currentUser) {
	  return (
		  <div className={styles.navauth}>
		<NavItem className="text-center text-md-right">
		<Link href="/Auth/Login"><a className={styles.a_link}>LOGIN</a></Link>
		</NavItem>
		<NavItem className="text-center text-md-right">
			<Link href="/Auth/Register"><a className={styles.a_link}>REGISTER</a></Link>
		</NavItem>
	</div>
	  );
	} else {
	  return (
		<div className={styles.navauth}>
		<NavItem className="text-center text-md-right">
		<Link href="/profile"><a className={styles.a_link}>Logged as: {currentUser?.email}</a></Link>
		<button onClick={updateScore} className={styles.btn}><a className={styles.score}>Your Score: {curUser?.score}</a></button>
	</NavItem>
	
	<NavItem className="text-center text-md-right">
		<button onClick={logout} className={styles.btn}><a className={styles.a_link}>LOGOUT</a></button>
	</NavItem>
	</div>
	  );
	}
  }

	return (
		<div>
		<Navbar className={styles.navbar} dark expand="lg">
			<NavbarBrand className={styles.a_link} ><Link href="/"><a>MICROGAMES</a></Link></NavbarBrand>
			<NavItem className="px-2 list-unstyled">
				<Link href="/globalLeaderboard"><a className={styles.a_link}>LEADERBOARD</a></Link>
			</NavItem>

			<NavbarToggler id="toggler" />
			<UncontrolledCollapse toggler="#toggler" navbar>
				<Nav className="justify-content-end" style={{ width: '100%' }} navbar>
					{renderAuthButton()}
				</Nav>
			</UncontrolledCollapse>
		</Navbar>
		<div className={styles.orange_line}></div>
		</div>
	);
};

export default NavBar;
