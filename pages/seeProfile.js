import Index from '../components/SeeProfile/index';
import { useDispatch, useSelector } from 'react-redux'
import Link from 'next/link'
import styles from '../styles/Profile.module.css'
import { db } from '../services/firebase-config';
import { useState, useEffect } from "react";
import { collection, getDocs } from "firebase/firestore";
import { useRouter } from 'next/router'

function SeeProfile() {
    const [curUser, setCurUser] = useState({})
    const usersCollectionRef = collection(db, "users")  
	const {currentUser} = useSelector((state) => state.user);
    const router = useRouter()
    console.log("query " + router.query.data);
    const seeUser = JSON.parse(router.query.data)

        return (
            <div className={styles.main}>
          <form className={styles.profileCard}>
            <h1 className={styles.profileTitle}>Username: {seeUser.userName}</h1>
            <p className={[styles.profileText, styles.mt].join(" ")}>Email: {seeUser.email}</p>
            <p className={[styles.profileText, styles.mb].join(" ")}>Rank: {seeUser.rank}</p>
            <p className={styles.profileText}>Total Score: {seeUser.score}</p>
            <p className={styles.profileText}>Suit Score: {seeUser.scoreSuit}</p>
            <p className={styles.profileText}>Dummy1 Score: {seeUser.scoreDummy1}</p>
            <p className={styles.profileText}>Dummy2 Score: {seeUser.scoreDummy2}</p>
            </form>
            </div>
        )
	
}

export default SeeProfile;