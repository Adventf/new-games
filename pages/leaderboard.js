import Board from '../components/leaderboard/index';
import styles from '../styles/Home.module.css'
import Link from 'next/link'
import {  useSelector } from 'react-redux'

function Leaderboard() {
	const {currentUser} = useSelector((state) => state.user);

	if (currentUser){return <div className={styles.main}>
		<Board />
		</div>;}
	else{return (
		<div className={styles.main}>
			<h2 className={styles.username}>You must log in first!</h2>
			<Link href="/Auth/Login">
				<h3 className={styles.card}>LOG IN</h3>
				</Link>
		</div>
	)}
}

export default Leaderboard;
