import Image from "next/image"
import Link from "next/link"
import styles from '../styles/Error.module.css'

export default function Custom404() {
   
  return (
    <div>
      <Image className={styles.landingImage}
      src="/404.png"
      alt="Picture"
      layout="fill"
      objectFit="cover"
      objectPosition="center"
      />
      <div className={styles.goHome}>
        <Link href='/'>
                    <button 
                     className={styles.backHome} 
                     type="button"
                     id="btn-signup"
                    >
                    <i className='fas fa-user-plus'></i>
                    Back Home
                    </button>
                </Link>
      <h3 className={styles.landingText}>Sorry, we can't find what you're looking for.</h3>
    
    </div>
    </div>

    // <div>
    //     <h1 className={styles.title}>
    //       <span className={styles["title-content"]}>404</span>
    //       <span className={styles["title-second"]}>Not Found! </span>
    //       <span className={styles["title-third"]}>Sorry, we can't find what you're looking for. </span>
    //     </h1>
    // </div>
  )
}
