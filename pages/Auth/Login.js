import {useState, useEffect} from 'react'
import {useDispatch, useSelector} from 'react-redux';
import { useRouter } from 'next/router'
import Link from 'next/link'
import { facebookSignInInitiate, githubSignInInitiate, googleSignInInitiate, loginInitiate } from '../../redux/actions';
import Image from 'next/image'
import {
  Form,
  Input,
} from 'reactstrap';
import styles from '../../styles/Auth.module.css'

const Login = () => {
    const [state, setState] = useState({
        email: "",
        password: "",
    })

    const {email, password} = state;

    const {currentUser} = useSelector((state) => state.user);

    const router = useRouter();

    useEffect(() => {
      if(currentUser) {
        router.push('/');
      }
    }, [currentUser, router])

    const dispatch = useDispatch();

    const handleGoogleSignIn = () => {
      dispatch(googleSignInInitiate())
    };

    const handleFBSignIn = () => {
      dispatch(facebookSignInInitiate())

    };

    const handleGithubSignIn = () => {
      dispatch(githubSignInInitiate())
    }

    const handleSubmit = (e) => {
      e.preventDefault()
      if(!email || !password) {
        return;
      }
      dispatch(loginInitiate(email, password))
      setState({email: "", password: ""})
    };
    const handleChange = (e) => {
      let {name, value} = e.target;
      setState({ ...state, [name]: value });
    }
  return (
    <div className={styles.main}>
        <div id='log-forms'>
          <br></br>
          <h1 className={styles.title}
                    style={{ textAlign: 'center'}}
                >
                    Sign In
                </h1>
            <Form className={styles.login} onSubmit={handleSubmit}>
              <div className={styles.wrapper}>
                <div className={styles.left}>
                    <button 
                     className={styles['loginbutton-google']}
                     type='button'
                     onClick={handleGoogleSignIn}
                    >
                      <Image src="/google.png" width={20} height={20}  alt="google" className={styles.icon} />
                        <span>
                          <i className='fab fa-google-plus-g'>Google</i>
                        </span>
                    </button>
                    <button
                     className={styles['loginbutton-facebook']}
                     type='button'
                     onClick={handleFBSignIn}
                    >
                      <Image src="/facebook.png" width={30} height={30} alt="facebook" className={styles.icon} />
                        <span>
                          <i className="fab fa-facebook-f">Facebook</i>
                        </span>
                    </button>
                    <button
                     className={styles['loginbutton-github']}
                     type='button'
                     onClick={handleGithubSignIn}
                    >
                      <Image src="/github.png" width={30} height={30} alt="github" className={styles.icon}/>
                        <span>
                          <i className="fab fa-facebook-f">Github</i>
                        </span>
                    </button>
                </div>
                  <div className={styles.center}>
                    <div className={styles.line} />
                    <div className={styles.or}>OR</div>
                  </div>
                  <div className={styles.right}>
                  <Input
                    type='email'
                    id='inputEmail'
                    placeholder='Email Address'
                    name='email'
                    onChange={handleChange}
                    value={email}
                    required
                  />
                  <Input
                    type='password'
                    id='inputPassword'
                    placeholder='Password'
                    name='password'
                    onChange={handleChange}
                    value={password}
                    required
                  />
                <br />
                <button className={styles.btn} type="submit">
                    <i className="fas fa-sign-in-alt"> Sign In</i>
                </button>
                <hr />
                <p className={styles.ask}>Don't have an account?</p> 
                <Link href='/Auth/Register'>
                    <button 
                     className={styles.btn} 
                     type="button"
                     id="btn-signup"
                    >
                    <i className='fas fa-user-plus'></i>
                    Sign Up New Account
                    </button>
                </Link>
                </div>
                </div>
            </Form>
        </div>
        </div>
  )
}

export default Login