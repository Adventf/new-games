import {useState, useEffect} from 'react'
import {useDispatch, useSelector} from 'react-redux';
import { registerInitiate } from '../../redux/actions';
import { useRouter } from 'next/router';
import { db } from '../../services/firebase-config'
import Link from 'next/link'
import {
  Form,
  Input,
  Button
} from 'reactstrap';
import styles from '../../styles/Auth.module.css'
import {
  collection,
  getDocs,
  addDoc,
} from "firebase/firestore";


const Register = () => {
    const [state, setState] = useState({
        userName: "",
        email: "",
        password: "",
        passwordConfirm: "",
        rank: 0,
        score: 0,
        scoreSuit: 0,
        scoreDummy1: 0, 
        scoreDummy2: 0, 
    })

    const usersCollectionRef = collection(db, "users")

    const {currentUser} = useSelector((state) => state.user);

    const router = useRouter();

    useEffect(() => {
      if(currentUser) {
        router.push("/")
      }
    }, [currentUser, router])

    const dispatch = useDispatch();

    const {email, password, userName, passwordConfirm} = state;
    const handleSubmit = async (e) => {
      e.preventDefault()
      if(password !== passwordConfirm) {
        return;
      }
      await addDoc(usersCollectionRef, { email: email, userName: userName, rank: 0, score:0, scoreSuit: 0, scoreDummy1: 0, scoreDummy2: 0 });
      dispatch(registerInitiate(email, password, userName, 0, 0, 0, 0))
      setState({ email: "", userName: "", password: "", passwordConfirm: ""})
    };

    const registering = async() => {
      
      handleSubmit()
    }

    const handleChange = (e) => {
      let {name, value} = e.target;
      setState({ ...state, [name]: value });
    }
  return (
    <div className={styles.main}>
        <div className='App'>
          <br></br>
          <h1 className={styles.title}
                    style={{ textAlign: 'center'}}
                >
                    Sign Up
                </h1>
            <Form className={styles.register} onSubmit={handleSubmit}>
                <div className={styles.wrapper}>
         <div className={styles.right}>
                  <Input
                    type='text'
                    id='userName'
                    placeholder='Username'
                    name='userName'
                    onChange={handleChange}
                    value={userName}
                    required
                  />
                  <Input
                    type='email'
                    id='user-email'
                    placeholder='Email Address'
                    name='email'
                    onChange={handleChange}
                    value={email}
                    required
                  />
                  <Input
                    type='password'
                    id='inputPassword'
                    placeholder='Password'
                    name='password'
                    onChange={handleChange}
                    value={password}
                    required
                  />
                  <Input
                    type='password'
                    id='inputRePassword'
                    placeholder='Repeat Password'
                    name='passwordConfirm'
                    onChange={handleChange}
                    value={passwordConfirm}
                    required
                  />
                  <br />
                <Button className={styles.registerButton} type="submit">
                    <i className="fas fa-user-plus"> Sign Up </i>
                </Button>
                <Link href='/Auth/Login'>
                  <a className='fas fa-angle-left'> 
                  Back
                  </a>
                </Link>
                </div>
                 </div>
            </Form>
           
        </div>
        </div>
    
  )
}

export default Register