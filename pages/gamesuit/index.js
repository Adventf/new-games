import GamePages from "../../components/gamesuit/GameSuit"
import Link from "next/link"
import { useDispatch, useSelector } from 'react-redux'
import styles from '../../styles/Home.module.css'

const GameSuit = () => {
    const {currentUser} = useSelector((state) => state.user);

	if (currentUser){return <GamePages />;}
	else{return (
		<div className={styles.main}>
			<h2 className={styles.username}>To play, you must log in first!</h2>
			<Link href="/Auth/Login">
				<h3 className={styles.card}>LOG IN</h3>
				</Link>
		</div>
	)}
}

export default GameSuit;